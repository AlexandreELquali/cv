# El Quali Alexandre C.V 
******* 
![test](alex.jpg)
-  **Informations personnels**  
Age : 27   
Adresse : Levallois-perret  
Email : <alex.kuall@gmail.com>  
Téléphone : 0664164803   

## Experience Pro
***
### 2019-2020
Formation développeur Web ( Javascript ) à White Rabbit School
### 2018-2019
Entrepreneur Import-export à Amiel Paris : Missions
- Gestion/organisation des transports pour les envois nationaux et internationaux   
- Promotion marques Française auprès de professionnels 
- Préparation de documents pour l' export 
- Instructions de dédouanement 
- Facturation
- Gestion et suivi Comptables des commandes (etc,)
### 2017-2016
Assistant Architecte chez Negobois , Missions :
- Conception et réalisation plan 2d
- Conception et rendu réaliste 3d avec  Sketchup
### 2016-2017
Responsable administratif chez Negobois, Missions : 
- Mise en place informatique du logiciel de production 
- Tenue de l'agenda, prise de rendez-vous, suivi des échanges e-mails
- Organisation des réunions, des événements, des voyages
### 2015-2016
Artisan Boulanger Au Cordon Bleu : Missions
-   Production, fabrication et cuisson des élements basiques d'une boulangerie   
## Skills 
_____
- Javascript
- NodeJS
- React/Angular
- Sketchup 
- Photoshop
- Anglais/Japonais 
----
